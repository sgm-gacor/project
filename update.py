import os
import git

def update_repository(repo_url):
    try:
        repo = git.Repo(os.getcwd())
        origin = repo.remotes.origin
        origin.fetch() 
        origin.pull() 

    
        changed_files = repo.git.diff("HEAD@{upstream}").splitlines()

        if changed_files:
            print("Changes detected.")
            repo.git.checkout("--", ".") 
            print("Repository successfully updated.")
        else:
            print("Repository successfully updated.")
    except git.exc.InvalidGitRepositoryError:
        print("Invalid Git repository.")
    except git.exc.GitCommandError as e:
        print("Error during repository update:", e)

if __name__ == "__main__":
    repository_url = "https://gitlab.com/sgm-gacor/project"
    update_repository(repository_url)
