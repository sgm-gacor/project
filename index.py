import os
from colorama import Fore, Style


print(f"""{Fore.GREEN}
 ██████╗██╗  ██╗███████╗███████╗    ████████╗ ██████╗  ██████╗ ██╗     ███████╗
██╔════╝██║  ██║██╔════╝██╔════╝    ╚══██╔══╝██╔═══██╗██╔═══██╗██║     ██╔════╝
██║     ███████║█████╗  █████╗         ██║   ██║   ██║██║   ██║██║     ███████╗
██║     ██╔══██║██╔══╝  ██╔══╝         ██║   ██║   ██║██║   ██║██║     ╚════██║
╚██████╗██║  ██║███████╗██║ {Fore.RED}Versi Beta{Fore.GREEN} ██║   ╚██████╔╝╚██████╔╝███████╗███████║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝            ╚═╝    ╚═════╝  ╚═════╝ ╚══════╝╚══════╝
""")

print(f"{Fore.GREEN}Welcome to Chef Tools Silakan Pilih Menu:")
print(f"{Fore.BLUE}[1] Path Checker (.git/.env/sftp/config)")
print(f"[2] Kcfinder Path Finder")
print(f"[3] Ojs Full Domain Path Finder")
print(f"[4] Ojs Remove 1 Level Path Finder")
print(f"[5] Register Member")
print(f"[6] CVE-2021-3129 (laravel ignition)🔥")
print(f"{Fore.RED}[10] Update Tools{Style.RESET_ALL}")

def screen_clear():
    os.system('cls')

data = {
    '1': 'run.py',
    '2': 'kcfinder.py',
    '3': 'ojs.py',
    '4': 'ojs1.py',
    '5': 'register.py',
    '6': 'ignition.py',
    '10': 'update.py',
}

SCRIPT_DIR = "tools"

print("")
pilihan = input("Pilih Angka 1/2/3/dst: ")

if pilihan == '10':
    current_directory = os.path.dirname(os.path.abspath(__file__))
    update_script_path = os.path.join(current_directory, data[pilihan])
    os.system(f'python3 {update_script_path}')
elif data.get(pilihan):
    full_path = os.path.join(SCRIPT_DIR, data[pilihan])
    file_to_check = input("Masukkan nama file yang ingin diperiksa: ")
    os.system(f'python3 {full_path} {file_to_check}')
else:
    print('Tidak ada pilihan yang valid.')
