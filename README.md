![Alt Text](image1.png)
```
pip install gitpython
git clone https://gitlab.com/sgm-gacor/project.git
cd project
python3 index.py
```
Pastikan Anda memiliki Python 3 terinstal di sistem Anda sebelum menjalankan langkah terakhir.

## Penyimpanan Domain

Pastikan untuk menyimpan daftar domain di dalam folder `/list` sebelum menjalankan aplikasi.

## Tentang Project

Tools ini dibuat open source dan dikembangkan secara kolaboratif. Versi yang Anda gunakan adalah versi terbaru yang di-host di repositori GitLab.

Jika Anda menemukan bug atau ingin memberikan kontribusi, jangan ragu untuk membuka *issue* atau mengirimkan *pull request* melalui repositori proyek ini.

Terima kasih telah menggunakan aplikasi ini!
