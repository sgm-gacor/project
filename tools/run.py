import sys
import os
import requests
from urllib.parse import urlparse, urlunparse
from colorama import Fore, Style
from concurrent.futures import ThreadPoolExecutor
import warnings
import time

def add_http_to_domain(url):
    if not url.startswith(('http://', 'https://')):
        url = 'http://' + url
    return url

def add_path_to_domain(url, path):
    parsed_url = urlparse(url)
    new_path = parsed_url.path.rstrip('/') + path
    new_url = urlunparse((parsed_url.scheme, parsed_url.netloc, new_path, parsed_url.params, parsed_url.query, parsed_url.fragment))
    return new_url

def load_paths_from_file(file_path):
    with open(file_path, 'r') as file:
        paths = [line.strip() for line in file.readlines()]
    return paths

def check_vulnerability(url, paths_to_check):
    try:
        response = requests.get(url, timeout=5)  # Menggunakan verifikasi sertifikat SSL bawaan
        if 'refs/heads/master' in response.text:
            print(f"{Fore.GREEN}Vulnerability found git in: {url}{Style.RESET_ALL}")
            return url
        elif any(keyword in response.text for keyword in ['autoUpload', 'remote_path', 'environment']):
            print(f"{Fore.BLUE}Vulnerability found sftp in: {url}{Style.RESET_ALL}")
            return url
        elif any(keyword in response.text for keyword in ['DB_CONNECTION', 'APP_NAME']):
            print(f"{Fore.YELLOW}Vulnerability found env in: {url}{Style.RESET_ALL}")
            return url
        else:
            print(f"{Fore.RED}No vulnerability found in: {url}{Style.RESET_ALL}")
            return None
    except requests.RequestException as e:
        print(f"{Fore.RED}Error accessing URL: {url}, {str(e)}{Style.RESET_ALL}")
        return None

def main():
    if len(sys.argv) > 1:  # Jika ada argumen baris perintah
        file_to_check = sys.argv[1]  # Ambil argumen pertama sebagai path file yang akan diperiksa
    else:  # Jika tidak ada argumen baris perintah, beri pesan kesalahan
        print("Usage: python3 path_checker.py <file_to_check>")
        return

    paths_to_check = load_paths_from_file('path/path.txt')

    # Ubah jalur ke folder 'bung'
    file_to_check = os.path.join('list', file_to_check)

    with open(file_to_check, 'r') as file:
        urls = [line.strip() for line in file.readlines()]

    start = time.time()
    results = []

    with ThreadPoolExecutor() as executor:
        futures = [executor.submit(check_vulnerability, add_path_to_domain(add_http_to_domain(url.strip()), path), paths_to_check) for url in urls for path in paths_to_check]

        for future in futures:
            url = future.result()
            if url:
                results.append(url)

    end = time.time()
    script_time = end - start

    print(f"\33[93;1mScan took {script_time} seconds to complete\n")
    print("\33[91;1m### \33[93;1mResults \33[91;1m###\33[1;0m")

    if len(results) == 0:
        print("\33[94;1m!!! No Results !!!")
    else:
        for url in results:
            print(url)

    print("\33[91;1m##################################\033[0m")

if __name__ == "__main__":
    # Matikan peringatan RequestsDependencyWarning
    warnings.filterwarnings("ignore", category=requests.exceptions.RequestsDependencyWarning)
    # Matikan peringatan TLS
    requests.packages.urllib3.disable_warnings()
    main()
