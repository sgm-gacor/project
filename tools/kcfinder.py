import sys
import os
import asyncio
import aiohttp
import time
import socket
from colorama import Fore, Style

async def fetch(url, session):
    try:
        async with session.get(url, timeout=3) as response:
            status = response.status
            response_text = await response.text()

            if status == 200:
                if "kcfinder" in response_text:
                    print("\33[97;1mFound It >->  \33[1;0m{}\33[97;1m  Status {}".format(response.url, status))
                    yay.append(response.url)
                else:
                    print("\33[95;1m??? {} Status {}".format(response.url, status))
            elif status == 404:
                print("\33[91;1mxXx \33[94;1m{}\33[91;1m Status {}".format(response.url, status))
            elif status == 403:
                print("\33[91;1mxXx \33[94;1m{}\33[91;1m Status \33[95;1m{}".format(response.url, status))
            else:
                print("\33[95;1m??? {} Status {}".format(response.url, status))
            
            if "Found It" in response_text:
                with open('vuln.txt', 'a') as vuln_file:
                    vuln_file.write(response.url + '\n')

            return await response.read()
    except asyncio.TimeoutError:
        print(f"\33[91;1mTimeout accessing {url}")
        return b''  # Return an empty response in case of a timeout
    except Exception as e:
        print(f"\33[91;1mError accessing {url}: {str(e)}")
        return b''  # Return an empty response in case of an error

async def process_targets(targets, paths, session):
    tasks = []
    for target in targets:
        if not target.startswith(("http://", "https://")):
            target = "http://" + target  # Add http:// if not present
        for path in paths:
            url = target + path
            task = asyncio.ensure_future(fetch(url, session))
            tasks.append(task)
    await asyncio.gather(*tasks)

async def run_in_batches(targets, paths, batch_size, session):
    for i in range(0, len(targets), batch_size):
        batch_targets = targets[i:i + batch_size]
        await process_targets(batch_targets, paths, session)

async def main():
    start = time.time()
    global yay
    yay = []

    if len(sys.argv) > 1:  # Jika ada argumen baris perintah
        file_to_check = sys.argv[1]  # Ambil argumen pertama sebagai path file yang akan diperiksa
    else:  # Jika tidak ada argumen baris perintah, beri pesan kesalahan
        print("Usage: python3 kcfinder.py <file_to_check>")
        return

    # Ubah jalur ke folder 'bung'
    file_to_check = os.path.join('list', file_to_check)

    # Read targets from specified file
    with open(file_to_check, 'r') as targets_file:
        targets = [line.strip() for line in targets_file if line.strip()]

    # Read paths from wordlist.txt
    with open('path/wordlist.txt', 'r') as admin_list:
        paths = [path.strip() for path in admin_list if path.strip()]

    batch_size = 10  # Adjust the batch size based on your system's capabilities

    # Use aiohttp.ClientSession directly
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(
        family=socket.AF_INET,
        ssl=False,  # Use ssl=False instead of verify_ssl=False
    )) as session:
        await run_in_batches(targets, paths, batch_size, session)

    end = time.time()
    script_time = end - start
    print("\33[93;1mScan took {} seconds to complete\n".format(script_time))
    print("\33[91;1m### \33[93;1mResults \33[91;1m###\33[1;0m")

    if len(yay) == 0:
        print("\33[94;1m!!! No Results !!!")
    else:
        for y in yay:
            print(y)

    print("\33[91;1m##################################\033[0m")

if __name__ == "__main__":
    asyncio.run(main())
